package bo.com.nextsoft.jcitunari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import bo.com.nextsoft.jcitunari.Activity.VideoDetailActivity;
import bo.com.nextsoft.jcitunari.Model.VideoModel;
import bo.com.nextsoft.jcitunari.R;

public class AdapterVideo extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<VideoModel> listdata;
    private Context context;
    private OnItemClickListener mOnItemClickListener;

    public AdapterVideo(Context context, List<VideoModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, VideoModel obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public TextView titleVideo;
        public ImageView imageVideo;
        public View lytParentVideo;

        public OriginalViewHolder(View v) {
            super(v);
            titleVideo = (TextView) v.findViewById(R.id.title_video);
            imageVideo = (ImageView) v.findViewById(R.id.image_video);
            lytParentVideo = (View) v.findViewById(R.id.lyt_bottom_video);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_video, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final VideoModel nm = listdata.get(position);

            StorageReference pathReference  = FirebaseStorage.getInstance().getReference().child(nm.getImage());
            GlideApp.with(context)
                    .load(pathReference)
                    .into(view.imageVideo);

            view.titleVideo.setText(nm.getTitle());

            view.lytParentVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, VideoDetailActivity.class);
                    intent.putExtra("title", nm.getTitle());
                    intent.putExtra("detail", nm.getDetail());
                    intent.putExtra("url", nm.getUrl());
                    context.startActivity(intent);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

}

