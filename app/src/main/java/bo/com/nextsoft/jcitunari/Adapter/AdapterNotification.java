package bo.com.nextsoft.jcitunari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import bo.com.nextsoft.jcitunari.Activity.NewEventActivity;
import bo.com.nextsoft.jcitunari.Model.NewModel;
import bo.com.nextsoft.jcitunari.R;

import java.util.List;

public class AdapterNotification extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<NewModel> listdata;
    private Context context;
    private OnItemClickListener mOnItemClickListener;

    public AdapterNotification(Context context, List<NewModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, NewModel obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        private TextView titleNews, subtitleNews, dateNews;
        private ImageView imageNews;
        private LinearLayout lytParentNews;

        public OriginalViewHolder(View v) {
            super(v);
            titleNews = (TextView) v.findViewById(R.id.title_news);
            subtitleNews = (TextView) v.findViewById(R.id.subtitle_news);
            dateNews = (TextView) v.findViewById(R.id.date_news);
            imageNews = (ImageView) v.findViewById(R.id.image_news);

            lytParentNews = (LinearLayout) v.findViewById(R.id.lyt_parent_news);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_card, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final NewModel nm = listdata.get(position);

            view.titleNews.setText(nm.getTitle());
            view.subtitleNews.setText(nm.getDetail());
            view.dateNews.setText(nm.getDate());

            StorageReference pathReference  = FirebaseStorage.getInstance().getReference().child(nm.getUrl());

            GlideApp.with(context)
                    .load(pathReference)
                    .into(view.imageNews);

            if(nm.getState() == true) {
                view.lytParentNews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, NewEventActivity.class);
                        intent.putExtra("title", nm.getTitle());
                        intent.putExtra("detail", nm.getDetail());
                        intent.putExtra("date", nm.getDate());
                        intent.putExtra("url", nm.getUrl());
                        intent.putExtra("description", nm.getDescription());
                        intent.putExtra("direction", nm.getDirection());
                        context.startActivity(intent);
                    }
                });
            }

        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

}

