package bo.com.nextsoft.jcitunari.Activity;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.*;
import bo.com.nextsoft.jcitunari.Adapter.AdapterNotification;
import bo.com.nextsoft.jcitunari.Model.NewModel;
import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;
import bo.com.nextsoft.jcitunari.utils.ViewAnimation;

import java.util.ArrayList;
import java.util.List;

public class NewActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private RecyclerView recyclerNotification;
    private AdapterNotification adapter;
    private List<NewModel> listNews;

    private final static int LOADING_DURATION = 200;

    public NewActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        recyclerNotification = (RecyclerView) findViewById(R.id.recyclerViewNew);

        recyclerNotification.setHasFixedSize(true);
        recyclerNotification.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        listNews = new ArrayList<>();

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_new);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Eventos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.teal_900);
    }

    private void initComponent() {

        final LinearLayout lyt_progress = (LinearLayout) findViewById(R.id.lyt_progress);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0f);

        db.collection("news").orderBy("order", Query.Direction.DESCENDING).limit(10).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot document : task.getResult()) {

                                NewModel newModel = new NewModel();
                                newModel.setId( document.getId() );
                                newModel.setTitle( document.getData().get("title").toString() );
                                newModel.setDetail( document.getData().get("detail").toString() );
                                newModel.setDate( document.getData().get("date").toString() );
                                newModel.setUrl( document.getData().get("url").toString() );
                                newModel.setDescription( document.getData().get("description").toString() );
                                newModel.setDirection( document.getData().get("direction").toString() );
                                newModel.setState( document.getBoolean("state") );
                                listNews.add(newModel);

                            }

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ViewAnimation.fadeOut(lyt_progress);
                                    adapter = new AdapterNotification(getApplicationContext(), listNews);
                                    recyclerNotification.setAdapter(adapter);

                                }
                            }, LOADING_DURATION);

                        } else {
                            Log.w("Actividad", "Error getting documents.", task.getException());
                        }
                    }
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
