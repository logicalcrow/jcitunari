package bo.com.nextsoft.jcitunari.Activity;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import bo.com.nextsoft.jcitunari.Adapter.AdapterVideo;
import bo.com.nextsoft.jcitunari.Model.VideoModel;
import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;
import bo.com.nextsoft.jcitunari.utils.ViewAnimation;
import bo.com.nextsoft.jcitunari.widget.SpacingItemDecoration;

public class VideoActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private AdapterVideo adapter;
    private RecyclerView recyclerVideo;
    private List<VideoModel> listVideo;

    private final static int LOADING_DURATION = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        recyclerVideo = (RecyclerView) findViewById(R.id.recyclerViewVideo);

        recyclerVideo.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 4), true));
        recyclerVideo.setHasFixedSize(true);
        recyclerVideo.setLayoutManager(new GridLayoutManager(this, 2));

        listVideo = new ArrayList<>();

        initToolbar();
        initRecyclerView();

    }

    public void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_video);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Videos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.teal_900);

    }

    public void initRecyclerView() {

        final LinearLayout lyt_progress = (LinearLayout) findViewById(R.id.lyt_progress_video);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0f);

        db.collection("videos").orderBy("order", Query.Direction.DESCENDING).limit(10).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot document : task.getResult()) {

                                VideoModel videoModel = new VideoModel();

                                videoModel.setId( document.getId() );
                                videoModel.setTitle( document.getData().get("title").toString() );
                                videoModel.setDetail( document.getData().get("detail").toString() );
                                videoModel.setImage( document.getData().get("image").toString() );
                                videoModel.setUrl( document.getData().get("url").toString() );

                                listVideo.add(videoModel);

                            }
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    ViewAnimation.fadeOut(lyt_progress);
                                    adapter = new AdapterVideo(getApplicationContext(), listVideo);
                                    recyclerVideo.setAdapter(adapter);

                                }
                            }, LOADING_DURATION);

                        } else {
                            Log.w("Actividad", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
