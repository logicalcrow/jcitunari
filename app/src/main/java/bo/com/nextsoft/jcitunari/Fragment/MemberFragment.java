package bo.com.nextsoft.jcitunari.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import bo.com.nextsoft.jcitunari.Adapter.AdapterPeopleInfo;
import bo.com.nextsoft.jcitunari.Model.PeopleModel;
import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;
import bo.com.nextsoft.jcitunari.utils.ViewAnimation;

public class MemberFragment extends Fragment {

    private View rootView;

    private RecyclerView recyclerViewInfo;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private AdapterPeopleInfo adapter;
    private List<PeopleModel> listPeople;
    private final static int LOADING_DURATION = 200;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_member, container, false);

        initToolbar("Miembros");
        initRecyclerView();

        return rootView;
    }

    public void initToolbar(String title) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar_member);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(title);
        Tools.setSystemBarColor(getActivity(), R.color.grey_900);
    }

    public void initRecyclerView() {

        recyclerViewInfo = (RecyclerView) rootView.findViewById(R.id.recyclerViewInfo);
        recyclerViewInfo.setHasFixedSize(true);
        recyclerViewInfo.setLayoutManager(new GridLayoutManager(getContext(), 1));

        listPeople = new ArrayList<>();

        final LinearLayout lyt_progress = (LinearLayout) rootView.findViewById(R.id.lyt_progress_info);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0f);

        db.collection("people").orderBy("order", Query.Direction.ASCENDING).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot document : task.getResult()) {

                                PeopleModel peopleModel = new PeopleModel();

                                peopleModel.setId( document.getId() );
                                peopleModel.setName( document.getData().get("name").toString() );
                                peopleModel.setPosition( document.getData().get("position").toString() );
                                peopleModel.setImage( document.getData().get("image").toString() );

                                listPeople.add(peopleModel);

                            }

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    ViewAnimation.fadeOut(lyt_progress);
                                    adapter = new AdapterPeopleInfo(getContext(), listPeople);
                                    recyclerViewInfo.setAdapter(adapter);

                                }
                            }, LOADING_DURATION);

                        } else {
                            Log.w("Actividad", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

}
