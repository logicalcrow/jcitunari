package bo.com.nextsoft.jcitunari.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;

public class InfoFragment extends Fragment {

    private View rootView;

    private ImageView navigationJcitunari, emailJcitunari, youtubeJcitunari, facebookJcitunari, instagramJcitunari;
    private LinearLayout privacy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_info, container, false);
        initToolbar("Información");

        privacy = (LinearLayout) rootView.findViewById(R.id.privacy);
        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.jcitunari.org/privacy");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        emailJcitunari = (ImageView) rootView.findViewById(R.id.emailJcitunari);
        emailJcitunari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{ "jcitunari@gmail.com" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Información JCI Tunari");
                startActivity(intent);
            }
        });

        navigationJcitunari = (ImageView) rootView.findViewById(R.id.navigationJcitunari);
        navigationJcitunari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.jcitunari.org");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        facebookJcitunari = (ImageView) rootView.findViewById(R.id.facebookJcitunari);
        facebookJcitunari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.facebook.com/jcitunari");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        instagramJcitunari = (ImageView) rootView.findViewById(R.id.instagramJcitunari);
        instagramJcitunari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.instagram.com/jcitunari");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        youtubeJcitunari = (ImageView) rootView.findViewById(R.id.youtubeJcitunari);
        youtubeJcitunari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.youtube.com/channel/UCxab4TuwuwClMC-PCKiV0pQ");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        return rootView;
    }

    public void initToolbar(String title) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar_info);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(title);
        Tools.setSystemBarColor(getActivity(), R.color.blue_grey_800);
    }

}
