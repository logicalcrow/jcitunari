package bo.com.nextsoft.jcitunari.Activity;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;

public class WhatJciActivity extends AppCompatActivity {

    private static final int MAX_STEP = 4;

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;

    private String title_array[] = {
            "¿Qué es la JCI?",
            "¿Qué es ser Ciudadano Activo?",
            "¿Cómo toman acción los miembros de la JCI?",
            "¿Eres un joven ciudadano activo?\n¿Deseas marcar una diferencia en tu comunidad?"
    };
    private String description_array[] = {
            "La JCI es una organización sin fines de lucro integrada por jóvenes ciudadanos activos de entre 18 y 40 años de edad que están involucrados y comprometidos a crear impacto en sus comunidades.",
            "Como miembros de la JCI, tomamos la iniciativa para resolver desafíos locales. No sólo pensamos en caridad o servicio; nos enfocamos en realizar un impacto sustentable a nivel local y global.",
            "Los miembros de la JCI analizan los desafíos locales, colaboran con asociados de la comunidad, realizan proyectos para encontrar soluciones y evalúan resultados para asegurar sustentabilidad.",
            "Únete a nosotros en este viaje de acción local para alcanzar un impacto global.",
    };
    private int about_images_array[] = {
            R.drawable.img_wizard_1,
            R.drawable.img_wizard_2,
            R.drawable.img_wizard_3,
            R.drawable.img_wizard_4,
    };
    private int color_array[] = {
            R.color.jci_primary,
            R.color.jci_primary,
            R.color.jci_primary,
            R.color.jci_primary
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_jci);

        bottomProgressDots(0);

        initComponent();

        Tools.setSystemBarColor(this, R.color.colorPrimaryDark);

    }

    private void bottomProgressDots(int current_index) {
        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[MAX_STEP];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 10;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.grey_80), PorterDuff.Mode.SRC_IN);
        }
    }


    private void initComponent() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                bottomProgressDots(position);
            }
        });

        ((ImageButton) findViewById(R.id.bt_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.item_article_stepper, container, false);
            TextView title = (TextView) view.findViewById(R.id.title_stepper);
            TextView detail = (TextView) view.findViewById(R.id.description_stepper);

            title.setText(title_array[position]);
            detail.setText(description_array[position]);
            ((ImageView) view.findViewById(R.id.image)).setImageResource(about_images_array[position]);
            ((RelativeLayout) view.findViewById(R.id.lyt_parent)).setBackgroundColor(getResources().getColor(color_array[position]));

            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return MAX_STEP;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


}
