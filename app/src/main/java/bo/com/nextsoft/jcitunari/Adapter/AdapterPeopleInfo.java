package bo.com.nextsoft.jcitunari.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import bo.com.nextsoft.jcitunari.Model.PeopleModel;
import bo.com.nextsoft.jcitunari.R;

public class AdapterPeopleInfo extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PeopleModel> listdata;
    private Context context;
    private OnItemClickListener mOnItemClickListener;

    public AdapterPeopleInfo(Context context, List<PeopleModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, PeopleModel obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        private TextView namePeopleInfo, positionPeopleInfo;
        private ImageView imagePeopleInfo;

        public OriginalViewHolder(View v) {
            super(v);
            namePeopleInfo = (TextView) v.findViewById(R.id.namePeopleInfo);
            positionPeopleInfo = (TextView) v.findViewById(R.id.positionPeopleInfo);
            imagePeopleInfo = (ImageView) v.findViewById(R.id.imagePeopleInfo);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_people_info, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            PeopleModel pm = listdata.get(position);

            view.namePeopleInfo.setText(pm.getName());
            view.positionPeopleInfo.setText(pm.getPosition());

            StorageReference pathReference  = FirebaseStorage.getInstance().getReference().child(pm.getImage());
            GlideApp.with(context)
                    .load(pathReference)
                    .into(view.imagePeopleInfo);

        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

}

