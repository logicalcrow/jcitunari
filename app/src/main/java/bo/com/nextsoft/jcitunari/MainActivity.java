package bo.com.nextsoft.jcitunari;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import bo.com.nextsoft.jcitunari.Fragment.MemberFragment;
import bo.com.nextsoft.jcitunari.Fragment.DeveloperFragment;
import bo.com.nextsoft.jcitunari.Fragment.HandBookFragment;
import bo.com.nextsoft.jcitunari.Fragment.HomeFragment;
import bo.com.nextsoft.jcitunari.Fragment.InfoFragment;

public class MainActivity extends AppCompatActivity {

    private Fragment fragment = null;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().performIdentifierAction(R.id.navigation_home, 0);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    navigation.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case R.id.navigation_dashboard:
                    fragment = new HandBookFragment();
                    navigation.setBackgroundColor(getResources().getColor(R.color.teal_600));
                    break;
                    case R.id.navigation_member:
                    fragment = new MemberFragment();
                    navigation.setBackgroundColor(getResources().getColor(R.color.grey_700));
                    break;
                case R.id.navigation_notifications:
                    fragment = new InfoFragment();
                    navigation.setBackgroundColor(getResources().getColor(R.color.blue_grey_700));
                    break;
                case R.id.navigation_developer:
                    fragment = new DeveloperFragment();
                    navigation.setBackgroundColor(getResources().getColor(R.color.red_800));
                    break;
            }

            return loadFragment(fragment);
        }
    };

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}
