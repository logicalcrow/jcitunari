package bo.com.nextsoft.jcitunari.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;

public class DeveloperFragment extends Fragment {

    private View rootView;
    private ImageView navigationLogicalcrow, youtubeLogicalcrow, facebookLogicalcrow, instagramLogicalcrow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_developer, container, false);
        initToolbar("Desarrollado por");

        navigationLogicalcrow = (ImageView) rootView.findViewById(R.id.navigationLogicalcrow);
        navigationLogicalcrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.logicalcrow.com");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        facebookLogicalcrow = (ImageView) rootView.findViewById(R.id.facebookLogicalcrow);
        facebookLogicalcrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.facebook.com/logicalcrow");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        instagramLogicalcrow = (ImageView) rootView.findViewById(R.id.instagramLogicalcrow);
        instagramLogicalcrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.instagram.com/logicalcrow");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        youtubeLogicalcrow = (ImageView) rootView.findViewById(R.id.youtubeLogicalcrow);
        youtubeLogicalcrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.youtube.com/channel/UC4sZOyPVzZaIPyFPpXkXw8w");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        return rootView;
    }

    public void initToolbar(String title) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(title);
        Tools.setSystemBarColor(getActivity(), R.color.red_900);
    }

}
