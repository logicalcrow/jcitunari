package bo.com.nextsoft.jcitunari.Activity;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;

public class WhatJciTunariActivity extends AppCompatActivity {

    private static final int MAX_STEP = 3;

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;

    private String title_array[] = {
            "¿Qué es la JCI Tunari?",
            "¿Qué proyectos realiza la JCI Tunari?",
            "¿Cómo formo parte de la JCI Tunari?",
    };
    private String description_array[] = {
            "La JCI Tunari es una organización local de jóvenes ciudadanos activos del departamento de Cochabamba que forma parte de la JCI.",
            "Realiza proyectos en marco a los 17 Objetivos de Desarrollo Sostenible planteados por la ONU.",
            "Toda persona que este en el rango de los 18 a 40 años que cumpla el proceso de postulante puede formar parte de la JCI Tunari"
    };
    private int about_images_array[] = {
            R.drawable.img_wizard_jci_tunari,
            R.drawable.img_wizard_jci_tunari,
            R.drawable.img_wizard_jci_tunari,
    };
    private int color_array[] = {
            R.color.jci_primary,
            R.color.jci_primary,
            R.color.jci_primary,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_jci);

        bottomProgressDots(0);

        initComponent();

        //Tools.setSystemBarTransparent(this);
        Tools.setSystemBarColor(this, R.color.colorPrimaryDark);
        //Tools.setSystemBarLight(this);
    }

    private void bottomProgressDots(int current_index) {
        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        ImageView[] dots = new ImageView[MAX_STEP];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 10;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[current_index].setImageResource(R.drawable.shape_circle);
            dots[current_index].setColorFilter(getResources().getColor(R.color.grey_80), PorterDuff.Mode.SRC_IN);
        }
    }


    private void initComponent() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                bottomProgressDots(position);
            }
        });

        ((ImageButton) findViewById(R.id.bt_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.item_article_stepper, container, false);
            TextView title = (TextView) view.findViewById(R.id.title_stepper);
            TextView detail = (TextView) view.findViewById(R.id.description_stepper);

            title.setText(title_array[position]);
            detail.setText(description_array[position]);
            ((ImageView) view.findViewById(R.id.image)).setImageResource(about_images_array[position]);
            ((RelativeLayout) view.findViewById(R.id.lyt_parent)).setBackgroundColor(getResources().getColor(color_array[position]));

            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return MAX_STEP;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

}
