package bo.com.nextsoft.jcitunari.Activity;

import android.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import static com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import static com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import static com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import static com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;


import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;
import bo.com.nextsoft.jcitunari.utils.YouTubeClass;


public class VideoDetailActivity extends YouTubeBaseActivity implements OnInitializedListener {

    private static final String API_KEY = "AIzaSyBG1j5vCj22KXOEbXVEOCkiRamPIn1EQUU";

    //https://www.youtube.com/watch?v=<VIDEO_ID>
    private static final String VIDEO_ID = "Rl9-MnVRRWE";
    private YouTubePlayerView youTubePlayerView;
    private TextView titleVideo, detailVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        //vidView = (VideoView) findViewById(R.id.myVideo);
        titleVideo = (TextView) findViewById(R.id.title_detail_video);
        detailVideo = (TextView) findViewById(R.id.detail_video);


        YouTubeClass youTubeClass = new YouTubeClass();

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_video);
        youTubePlayerView.initialize(API_KEY, youTubeClass);


        initToolbar();
        initComponent();

    }

    public void initToolbar() {
        //ActionBar actionBar = getSupportActionBar();

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_new);
        //toolbar.setTitle("Eventos");
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.teal_900);

    }


    public void initComponent() {

        /*Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String title = extras.getString("title");
        String detail = extras.getString("detail");

        titleVideo.setText(title);
        detailVideo.setText(detail);*/

        //vidAddress = "https://www.youtube.com/watch?v=Rl9-MnVRRWE";
        //vidUri = Uri.parse(vidAddress);

        //vidView.setVideoURI(vidUri);
        //vidView.start();


        //MediaController vidControl = new MediaController(this);
        //vidControl.setAnchorView(vidView);
        //vidView.setMediaController(vidControl);



    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if(null== youTubePlayer) return;

        // Start buffering
        if (!b) {
            youTubePlayer.cueVideo(VIDEO_ID);
        }

        // Add listeners to YouTubePlayer instance
        youTubePlayer.setPlayerStateChangeListener(new PlayerStateChangeListener() {
            @Override public void onAdStarted() { }
            @Override public void onError(ErrorReason arg0) { }
            @Override public void onLoaded(String arg0) { }
            @Override public void onLoading() { }
            @Override public void onVideoEnded() { }
            @Override public void onVideoStarted() { }
        });


        youTubePlayer.setPlaybackEventListener(new PlaybackEventListener() {
            @Override public void onBuffering(boolean arg0) { }
            @Override public void onPaused() { }
            @Override public void onPlaying() { }
            @Override public void onSeekTo(int arg0) { }
            @Override public void onStopped() { }
        });

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
