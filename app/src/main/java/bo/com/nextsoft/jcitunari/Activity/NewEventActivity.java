package bo.com.nextsoft.jcitunari.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import bo.com.nextsoft.jcitunari.Adapter.GlideApp;
import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;

public class NewEventActivity extends AppCompatActivity {

    private TextView titleNewEvent, detailNewEvent, dateNewEvent, descriptionNewEvent, directionNewEvent;
    private ImageView imageNewEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        titleNewEvent = (TextView) findViewById(R.id.titleNewEvent);
        detailNewEvent = (TextView) findViewById(R.id.detailNewEvent);
        dateNewEvent = (TextView) findViewById(R.id.dateNewEvent);

        descriptionNewEvent = (TextView) findViewById(R.id.descriptionNewEvent);
        directionNewEvent = (TextView) findViewById(R.id.directionNewEvent);
        imageNewEvent = (ImageView) findViewById(R.id.imageNewEvent);

        initToolbar();
        initContent();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.teal_900);
    }

    private void initContent() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String title = extras.getString("title");
        String detail = extras.getString("detail");
        String date = extras.getString("date");
        String description = extras.getString("description");
        String direction = extras.getString("direction");
        String url = extras.getString("url");

        titleNewEvent.setText(title);
        detailNewEvent.setText(detail);
        dateNewEvent.setText(date);

        descriptionNewEvent.setText(description);
        directionNewEvent.setText(direction);
        StorageReference pathReference  = FirebaseStorage.getInstance().getReference().child(url);
        GlideApp.with(this)
                .load(pathReference)
                .into(imageNewEvent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
