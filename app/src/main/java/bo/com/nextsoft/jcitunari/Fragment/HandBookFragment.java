package bo.com.nextsoft.jcitunari.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import bo.com.nextsoft.jcitunari.Activity.NewActivity;
import bo.com.nextsoft.jcitunari.Activity.VideoActivity;
import bo.com.nextsoft.jcitunari.Activity.WhatJciActivity;
import bo.com.nextsoft.jcitunari.Activity.WhatJciTunariActivity;
import bo.com.nextsoft.jcitunari.R;
import bo.com.nextsoft.jcitunari.utils.Tools;

public class HandBookFragment extends Fragment {

    private View rootView;
    private FloatingActionButton whatJciClick, whatJciTunariClick, notificationClick, videoClick;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_hand_book, container, false);
        initToolbar("Información y Actividades");

        whatJciClick = (FloatingActionButton) rootView.findViewById(R.id.what_jci_click);
        whatJciClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), WhatJciActivity.class);
                startActivity(intent);

            }
        });

        whatJciTunariClick = (FloatingActionButton) rootView.findViewById(R.id.what_jci_tunari_click);
        whatJciTunariClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), WhatJciTunariActivity.class);
                startActivity(intent);
            }
        });

        notificationClick = (FloatingActionButton) rootView.findViewById(R.id.notification_click);
        notificationClick.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(getContext(), NewActivity.class);
                startActivity(intent);
            }
        });

        videoClick = (FloatingActionButton) rootView.findViewById(R.id.video_jci_click);
        videoClick.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(getContext(), VideoActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

    public void initToolbar(String title) {

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar_hand);
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(title);
        Tools.setSystemBarColor(getActivity(), R.color.teal_900);

    }

}
